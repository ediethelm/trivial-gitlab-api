;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(defsystem :trivial-gitlab-api
  :name "trivial-gitlab-api"
  :description ""
  :version "0.1.1"
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "MIT"
  :depends-on (:trivial-utilities
               :trivial-json-codec
	       :log4cl
	       :iterate
	       :babel
	       :drakma
	       :cl-base64)
  :in-order-to ((test-op (test-op :trivial-gitlab-api/test)))
  :components ((:file "package")
	       (:file "trivial-gitlab-api")))

(defsystem :trivial-gitlab-api/test
  :name "trivial-timer/test"
  :description "Unit Tests for the trivial-gitlab-api project."
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "MIT"
  :depends-on (:trivial-gitlab-api fiveam)
  :perform (test-op (o s) (uiop:symbol-call :fiveam  '#:run! :trivial-gitlab-api-tests))
  :components ((:file "test-trivial-gitlab-api")))


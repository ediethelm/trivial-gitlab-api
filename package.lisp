(uiop:define-package #:trivial-gitlab-api
  (:documentation "")
  (:use #:common-lisp)
  (:export #:clone-project
	   #:pull-project
	   #:clean-project
	   #:revert-project
	   #:discover-asdf-projects))


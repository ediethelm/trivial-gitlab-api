(in-package :trivial-gitlab-api)

(defun build-project-path (root-path project &optional (group nil))
  (let* ((group-path (if (or (null group)
			     (string= "" group))
			 root-path
			 (uiop:merge-pathnames* (format nil "~a/" group) root-path))))
    (return-from build-project-path
      (values (uiop:merge-pathnames* (format nil "~a/" project) group-path)))))

(defun project-path-exists (root-path project &optional (group nil))
  (let* ((project-path (build-project-path root-path project group)))
    (return-from project-path-exists
      (the boolean (values (not (null (uiop:probe-file* project-path))))))))

(defun pull-project (project group root-path &key (is-project-path nil))
  (let* ((project-path (if is-project-path root-path (build-project-path root-path project group))))

    (unless (uiop:probe-file* project-path)
      (format t "Directory ~a does not exist." project-path)
      (return-from pull-project nil))

    (format t "Pulling ~a in ~a~%" project project-path)
    (uiop:run-program (format nil "git pull ~a" project-path)))
  (return-from pull-project t))


(defun revert-project (project group root-path &key (is-project-path nil))
  (let* ((project-path (if is-project-path root-path (build-project-path root-path project group))))

    (unless (uiop:probe-file* project-path)
      (format t "Directory ~a does not exist." project-path)
      (return-from revert-project nil))

    (format t "Reverting ~a in ~a~%" project project-path)
    (uiop:run-program (format nil "cd ~a && git reset --hard @{u}" project-path))
    (uiop:run-program (format nil "cd ~a && git clean -dfx" project-path))
    (uiop:run-program (format nil "cd ~a && git pull" project-path)))
  (return-from revert-project t))

(defun clean-project (project group root-path &key (is-project-path nil))
  (let* ((project-path (if is-project-path root-path (build-project-path root-path project group))))

    (unless (uiop:probe-file* project-path)
      (format t "Directory ~a does not exist." project-path)
      (return-from clean-project nil))

    (format t "Cleaning ~a in ~a~%" project project-path)
    (uiop:run-program (format nil "cd ~a && git clean -dfx" project-path)))
  (return-from clean-project t))


(defun clone-project (username project group root-path &key (is-project-path nil))
  (let* ((project-path (if is-project-path root-path (build-project-path root-path project group))))

    (when (uiop:probe-file* project-path)
      (format t "Not overwriting existing local directory ~a" project-path)
      (return-from clone-project nil))

    (uiop:ensure-pathname project-path :ensure-directories-exist t)

    (format t "Cloning ~a into ~a~%" project project-path)
    (uiop:run-program (format nil "git clone ~a ~a"
			      (concatenate 'string
					   "https://gitlab.com/"
					   username
					   (when (and group (string/= group "")) "/")
					   group
					   "/"
					   project
					   ".git")
			      project-path)))
  (return-from clone-project t))

(defun get-projects-in-group (group-id &key (access-token nil) (private-token nil))
  (when (and access-token private-token)
    (log:error "Only one of access-token and private-token can be provided.")
    (cerror "Return nil." "Only one of access-token and private-token can be provided.")
    (return-from get-projects-in-group (values nil)))

  (let ((gitlab-group-response (drakma:http-request
				(format nil "https://gitlab.com/api/v4/groups/~a~a"
					group-id
					(cond (access-token (format nil "?access_token=~a" access-token))
					      (private-token (format nil "?private_token=~a" private-token))
					      (t "")))))
	(response nil))

    (ignore-errors
      (setf response (trivial-json-codec:deserialize-raw (babel:octets-to-string gitlab-group-response))))

    (unless response
      (log:error "Got an invalid response from GitLab.")
      (return-from get-projects-in-group nil))

    (let ((projects-response (cadar (member :projects response :key #'car))))
      ;; collect id, path and http_url_to_repo of all projects
      (return-from get-projects-in-group
	(iterate:iterate
	  (iterate:for project in-vector projects-response)
	  (iterate:collect (list :project-id (cadar (member :id project  :key #'car))
				 :project-path (cadar (member :path project  :key #'car ))
				 :project-repo-url (cadar (member :http_url_to_repo project  :key #'car )))))))))

(defun extract-system-info (asd-contents)
  (let ((parsed (read-from-string (format nil "~{~a~}"
					  (with-input-from-string (stream asd-contents)
					    (loop for line = (read-line stream nil)
					       with started = nil
					       while line
					       when (and (not started)
							 (> (length line) (length "(defsystem"))
							 (string-equal "(defsystem" line :end1 (length "(defsystem") :end2 (length "(defsystem")))
					         do (progn
						      (setf started t)
						      (setf line (format nil "(:~a~%" (subseq line 1))))

					       when (and started
							 (> (length line) 0)
							 (not (eq #\; (char line 0))))
					         collect (format nil "~a~%" line)))))))

    (list :project-defsys (cadr parsed)
	  :project-name (getf parsed :name)
	  :project-version (getf parsed :version)
	  :project-description (getf parsed :description)
	  :project-author (getf parsed :author))))

(defun get-project-asdf-information (project-id project-path project-repo-url &key (access-token nil) (private-token nil))
  (when (and access-token private-token)
    (log:error "Only one of access-token and private-token can be provided.")
    (cerror "Return nil." "Only one of access-token and private-token can be provided.")
    (return-from get-project-asdf-information (values nil)))

  (let* ((gitlab-project-response (drakma:http-request
				   (format nil "https://gitlab.com/api/v4/projects/~a/repository/files/~a?ref=master~a"
					   project-id
					   (concatenate 'string project-path ".asd")
					   (cond (access-token (format nil "&access_token=~a" access-token))
						 (private-token (format nil "&private_token=~a" private-token))
						 (t "")))))
	 (contents nil)
	 (response nil))

    (ignore-errors
      (setf response (trivial-json-codec:deserialize-raw (babel:octets-to-string gitlab-project-response))))

    (unless response
      (log:error "Got an invalid response from GitLab.")
      (return-from get-project-asdf-information nil))

    (setf contents (cadar (member :content response :key #'car)))

    (unless contents
      (log:error "The GitLab project '~a' does not contain as ASDF file (~a)." project-id (concatenate 'string project-path ".asd"))
      (return-from get-project-asdf-information nil))

    (append (extract-system-info
	     (cl-base64:base64-string-to-string contents))
	    (list :project-id project-id :project-repo-url project-repo-url))))



(defun discover-asdf-projects (group-id &key (access-token nil) (private-token nil))
  ""
  (when (and access-token private-token)
    (log:error "Only one of access-token and private-token can be provided.")
    (cerror "Return nil." "Only one of access-token and private-token can be provided.")
    (return-from discover-asdf-projects (the list nil)))

  (let ((projects (get-projects-in-group
		   group-id
		   :access-token access-token
		   :private-token private-token)))
    ;; Create a temporary folder
    (let ((collected-projects nil))
      (iterate:iterate
	(iterate:for project in projects)
	(push (get-project-asdf-information
	       (getf project :project-id)
	       (getf project :project-path)
	       (getf project :project-repo-url)
	       :access-token access-token
	       :private-token private-token)
	      collected-projects))

      (return-from discover-asdf-projects (the list collected-projects)))))
